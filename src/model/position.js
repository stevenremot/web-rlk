import { RlkComponent } from './component';
import { defineElement } from '../custom-elements';

export class RlkPosition extends RlkComponent {
  static get is() {
    return 'rlk-position';
  }

  get x() {
    return +this.getAttribute('x');
  }

  set x(x) {
    return this.setAttribute('x', +x);
  }

  get y() {
    return +this.getAttribute('y');
  }

  set y(y) {
    return this.setAttribute('y', +y);
  }
}

defineElement(RlkPosition);
