import { defineElement } from '../custom-elements';

export class RlkEntity extends HTMLElement {
  static get is() {
    return 'rlk-entity';
  }
}

defineElement(RlkEntity);
