import { defineElement } from '../custom-elements';

export class RlkLevel extends HTMLElement {
  static get is() {
    return 'rlk-level';
  }

  get ground() {
    return this.querySelector('rlk-ground')
  }

  /**
   * Return a bidimensionnal array describing each cell: the ground
   * and the entities on it.
   */
  getCellsDescription() {
    const ground = this.ground;
    const levelWidth = ground.width;
    const levelHeight = ground.height;
    const groundCells = ground.cells;

    const positions = Array.from(this.querySelectorAll('rlk-position'))
      .filter(pos => pos.entity !== ground.entity);

    const cellsDescriptions = [];

    for (let x = 0; x < levelWidth; x += 1) {
      const column = [];

      for (let y = 0; y < levelHeight; y += 1) {
        column.push({
          ground: groundCells.cellAt(x, y),
          entities: positions.filter(pos => pos.x === x && pos.y === y).map(pos => pos.entity),
        });
      }

      cellsDescriptions.push(column);
    }

    return cellsDescriptions;
  }
}

defineElement(RlkLevel);
