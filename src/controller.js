import { defineElement } from './custom-elements';

import { canPassAt, getNavPath } from './navigation';

const PATH_FINDING_FPS = 12;

const DIRECTION_BY_EVT = {
  'rlk:move-up': [0, -1],
  'rlk:move-down': [0, 1],
  'rlk:move-left': [-1, 0],
  'rlk:move-right': [1, 0]
};

export class RlkController extends HTMLElement {
  static get is() {
    return 'rlk-controller';
  }

  get model() {
    return document.querySelector(this.getAttribute('model'));
  }

  get hero() {
    return this.model.querySelector('rlk-hero');
  }

  get ground() {
    return this.model.querySelector('rlk-ground');
  }

  /**
   * @private
   */
  tick() {
    this.dispatchEvent(new CustomEvent('rlk:tick', { bubbles: true }), this);
  }

  handleEvent(evt) {
    this.stopPathFinding();

    if (evt.type in DIRECTION_BY_EVT) {
      const { hero } = this;
      const { position } = hero;
      const [dx, dy] = DIRECTION_BY_EVT[evt.type];
      const newX = position.x + dx;
      const newY = position.y + dy;

      this.tryMoveTo(newX, newY);
    }
  }

  tryMoveTo(newX, newY) {
      if (canPassAt(this.model, newX, newY)) {
        this.hero.position.x = newX;
        this.hero.position.y = newY;
        this.tick();
        return true;
      }
    return false;
  }

  handlePressEvt(evt) {
    const navPath = getNavPath(this.model, this.hero.position, evt.detail);

    if (navPath) {
      this.path = navPath.slice(1);
    }
  }

  handlePathFinding() {
    if (this.path && this.path.length > 0) {
      const { x, y } = this.path.shift();
      if (!this.tryMoveTo(x, y)) {
        this.stopPathFinding();
      }
    }
  }

  stopPathFinding() {
    this.path = null;
  }

  connectedCallback() {
    this.pathLoop = setInterval(
      this.handlePathFinding.bind(this),
      1000 / PATH_FINDING_FPS
    );
    this.path = null;

    const handleEvent = this.handleEvent.bind(this);
    Object.keys(DIRECTION_BY_EVT).forEach(evtType =>
      document.addEventListener(evtType, handleEvent)
    );
    document.addEventListener('rlk:cell-press', this.handlePressEvt.bind(this));
  }
}

defineElement(RlkController);
