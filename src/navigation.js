function hasPositionAt(level, x, y) {
  return Array.from(level.querySelectorAll('.body rlk-position')).some(
    position => position.x === x && position.y === y
  );
}

export function canPassAt(level, x, y) {
  const ground = level.querySelector('rlk-ground');

  return ground.canPassAt(x, y) && !hasPositionAt(level, x, y);
}

// Path finding

function getLevelNavMask(level) {
  const cells = level.getCellsDescription();
  return {
    width: cells.length,
    height: cells[0].length,
    canPass(x, y) {
      return (
        level.ground.canPassAt(x, y) &&
        !cells[x][y].entities.some(entity => entity.classList.contains('body'))
      );
    }
  };
}

function getDistance(from, to) {
  return Math.abs(from.x - to.x) + Math.abs(from.y + to.y);
}

function compareNodes(node1, node2) {
  return node1.cost + node1.estimate - (node2.cost + node2.estimate);
}

function getMin(items, compareFunc) {
  return items.reduce(
    (min, newItem) => (compareFunc(min, newItem) < 0 ? min : newItem)
  );
}

const directions = [[0, 1], [0, -1], [1, 0], [-1, 0]];

function getNeighbours(node, mask, to) {
  return directions
    .map(([x, y]) => [x + node.point.x, y + node.point.y])
    .filter(([x, y]) => x >= 0 && x < mask.width && y >= 0 && y < mask.height)
    .filter(([x, y]) => mask.canPass(x, y))
    .map(([x, y]) => ({
      point: { x, y },
      cost: node.cost + 1,
      estimate: getDistance({ x, y }, to),
      parent: node
    }));
}

function getClosedListDuplicate(node, closedList) {
  const duplicates = closedList.filter(
    elt => elt.point.x === node.point.x && elt.point.y === node.point.y
  );

  if (duplicates.length > 0) {
    return getMin(duplicates, compareNodes);
  }

  return null;
}

function buildNodeList(node) {
  if (node.parent) {
    return [...buildNodeList(node.parent), node];
  }

  return [node];
}

function isBetterThanList(list, node) {
  const duplicate = getClosedListDuplicate(node, list);
  return !duplicate || compareNodes(duplicate, node) > 0;
}

export function getNavPath(level, from, to) {
  const mask = getLevelNavMask(level);
  const openList = [
    {
      point: from,
      cost: 0,
      estimate: getDistance(from, to),
      parent: null
    }
  ];
  const closedList = [];
  let resultNode;

  while (openList.length > 0 && !resultNode) {
    const node = getMin(openList, compareNodes);
    openList.splice(openList.indexOf(node), 1);

    if (node.point.x === to.x && node.point.y === to.y) {
      resultNode = node;
    } else {
      getNeighbours(node, mask, to)
        .filter(neighbour => isBetterThanList(openList.concat(closedList), neighbour))
        .forEach(neighbour => openList.push(neighbour));
      closedList.push(node);
    }
  }

  if (!resultNode) {
    return null;
  }

  return buildNodeList(resultNode).map(node => node.point);
}
