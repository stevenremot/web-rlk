import { defineElement } from '../custom-elements';

import { canPassAt } from '../navigation';

const DIRECTIONS = [[1, 0], [-1, 0], [0, 1], [0, -1]];

export class RlkAiRandom extends HTMLElement {
  static get is() {
    return 'rlk-ai-random';
  }

  get entity() {
    return this.parentNode;
  }

  get position() {
    return this.entity.querySelector('rlk-position');
  }

  get level() {
    return this.entity.parentNode;
  }

  connectedCallback() {
    document.addEventListener('rlk:tick', () => this.handleTick());
  }

  handleTick() {
    const directions = [...DIRECTIONS];
    let hasDirection = false;
    const { position } = this;

    while (!hasDirection && directions.length > 0) {
      const index = Math.floor(Math.random() * directions.length);
      const [[dx, dy]] = directions.splice(index, 1);
      const newX = position.x + dx;
      const newY = position.y + dy;

      if (canPassAt(this.level, newX, newY)) {
        position.x = newX;
        position.y = newY;
        hasDirection = true;
      }
    }
  }
}

defineElement(RlkAiRandom);
