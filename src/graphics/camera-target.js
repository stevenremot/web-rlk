import { defineElement } from '../custom-elements';
import { RlkComponent } from '../model';

export class RlkCameraTarget extends RlkComponent {
  static get is() {
    return 'rlk-camera-target';
  }
}

defineElement(RlkCameraTarget);
