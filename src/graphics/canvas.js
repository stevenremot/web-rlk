import { defineElement } from '../custom-elements';

import classNames from './canvas.css';

const Y_X_RATIO = 1;

export class RlkCanvas extends HTMLElement {
  static get is() {
    return 'rlk-canvas';
  }

  constructor() {
    super();

    this.classList.add(classNames.canvas);
    this.canvas = document.createElement('canvas');
    this.appendChild(this.canvas);
    this.cellWidth = 20;
  }

  get cellHeight() {
    return this.cellWidth * Y_X_RATIO;
  }

  get centerX() {
    return this.canvas.width / 2;
  }

  get centerY() {
    return this.canvas.height / 2;
  }

  get model() {
    return document.querySelector(this.getAttribute('model'));
  }

  render() {
    this.canvas.width = this.offsetWidth;
    this.canvas.height = this.offsetHeight;
    const ctx = this.canvas.getContext('2d');
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    ctx.translate(this.canvas.width / 2, this.canvas.height / 2);

    ctx.scale(this.cellWidth, this.cellWidth);
    this.centerCamera(ctx);
    this.renderGround(ctx);
    this.renderObjects(ctx);
  }

  getCameraPosition() {
    const target = this.model.querySelector('rlk-camera-target');
    return target.entity.querySelector('rlk-position');
  }

  centerCamera(ctx) {
    const position = this.getCameraPosition();
    ctx.translate(-position.x, -position.y * Y_X_RATIO);
  }

  renderGround(ctx) {
    const ground = this.model.querySelector('rlk-ground');
    const renderable = ground.parentNode.querySelector('rlk-renderable');
    this.renderRenderable(ctx, renderable, { x: 0, y: 0 });
  }

  renderObjects(ctx) {
    const renderables = this.model.querySelectorAll('rlk-renderable[layer=objects]');

    for (const renderable of renderables) {
      const position = renderable.closest('rlk-entity').querySelector('rlk-position');
      this.renderRenderable(ctx, renderable, position);
    }
  }

  renderRenderable(ctx, renderable, position) {
    ctx.save();
    ctx.translate(position.x, position.y * Y_X_RATIO);
    renderable.render(ctx);
    ctx.restore();
  }

  startLoop() {
    requestAnimationFrame(() => {
      this.render();
      this.startLoop();
    });
  }

  connectedCallback() {
    this.startLoop();
  }
}

defineElement(RlkCanvas);
