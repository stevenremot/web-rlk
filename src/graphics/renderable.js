import { defineElement } from '../custom-elements';

export class RlkRenderable extends HTMLElement {
  static get is() {
    return 'rlk-renderable';
  }

  constructor(renderer, layer) {
    super();

    if (renderer) {
      renderer.setAttribute('slot', 'renderer');
      this.appendChild(renderer);
    }

    if (layer) {
      this.setAttribute('layer', layer);
    }
  }

  get renderer() {
    return this.querySelector('[slot=renderer]');
  }

  /**
   * @param {CanvasRenderingContext2D} ctx
   */
  render(ctx) {
    if (this.renderer) {
      ctx.save();
      this.renderer.render(ctx, { entity: this.parentNode });
      ctx.restore();
    }
  }
}

defineElement(RlkRenderable);
