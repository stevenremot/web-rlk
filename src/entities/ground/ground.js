import { RlkComponent } from '../../model/component';
import { defineElement } from '../../custom-elements';

export class RlkGround extends RlkComponent {
  static get is() {
    return 'rlk-ground';
  }

  get width() {
    return this.cells.width;
  }

  get height() {
    return this.cells.height;
  }

  get cells() {
    return this.querySelector('[slot=cells]');
  }

  canPassAt(x, y) {
    return x >= 0 && x < this.width && y >= 0 && y < this.height && this.cells.cellAt(x, y).canPass;
  }
}

defineElement(RlkGround);
