import { defineElement } from '../../custom-elements';

const X_Y_RATIO = 1;

export class RlkGroundRenderer extends HTMLElement {
  static get is() {
    return 'rlk-ground-renderer';
  }

  /**
   * @param {CanvasRenderingContext2D} ctx
   */
  render(ctx, { entity }) { // eslint-disable-line class-methods-use-this
    const { width, height, cells } = entity.querySelector('rlk-ground');

    for (let x = 0; x < width; x += 1) {
      for (let y = 0; y < height; y += 1) {
        ctx.save();
        ctx.fillStyle = cells.cellAt(x, y).color;
        ctx.translate(x, y * X_Y_RATIO);
        ctx.fillRect(0, 0, 1, X_Y_RATIO);
        ctx.restore();
      }
    }

    ctx.save();
    ctx.strokeStyle = 'rgba(0, 0, 0, 0.25)';
    ctx.lineWidth = 1 / 20;

    for (let x = 1; x < width; x += 1) {
      ctx.beginPath();
      ctx.moveTo(x, 0);
      ctx.lineTo(x, height * X_Y_RATIO);
      ctx.stroke();
    }

    for (let y = 1; y < height; y += 1) {
      ctx.beginPath();
      ctx.moveTo(0, y * X_Y_RATIO);
      ctx.lineTo(width, y * X_Y_RATIO);
      ctx.stroke();
    }

    ctx.restore();
  }
}

defineElement(RlkGroundRenderer);
