import { defineElement } from '../../custom-elements';

const CELL_BY_CHAR = {
  '#': {
    type: 'wall',
    canPass: false,
    color: '#696969'
  },
  '.': {
    type: 'ground',
    canPass: true,
    color: '#dddddd'
  },
  '_': {
    type: 'ground',
    canPass: true,
    color: '#8b4513'
  },
  '%': {
    type: 'ground',
    canPass: true,
    color: '#555566',
  }
}

export class RlkAsciiGround extends HTMLElement {
  static get is() {
    return 'rlk-ascii-ground';
  }

  constructor() {
    super();

    this.cells = this.textContent
      .split('\n')
      .map(line => Array.from(line.trim()))
      .filter(line => line.length > 0)
      .map(line => line.map(chr => CELL_BY_CHAR[chr]));
  }

  get width() {
    return this.cells[0].length;
  }

  get height() {
    return this.cells.length;
  }

  cellAt(x, y) {
    return this.cells[y][x];
  }
}

defineElement(RlkAsciiGround);
