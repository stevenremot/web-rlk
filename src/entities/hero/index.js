import { defineElement } from '../../custom-elements';

import { RlkPosition, RlkEntity } from '../../model';
import { RlkRenderable } from '../../graphics/renderable';
import { RlkHeroRenderer } from './renderer';
import { RlkCameraTarget } from '../../graphics';

export class RlkHero extends HTMLElement {
  static get is() {
    return 'rlk-hero';
  }

  constructor() {
    super();

    const entity = new RlkEntity();

    this.position = new RlkPosition();
    this.position.x = this.getAttribute('x');
    this.position.y = this.getAttribute('y');

    entity.appendChild(this.position);
    entity.appendChild(new RlkRenderable(new RlkHeroRenderer(), 'objects'));
    entity.appendChild(new RlkCameraTarget());

    entity.classList.add('body');
    this.appendChild(entity);
  }
}

defineElement(RlkHero);
