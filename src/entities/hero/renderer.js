import { defineElement } from '../../custom-elements';

export class RlkHeroRenderer extends HTMLElement {
  static get is() {
    return 'rlk-hero-renderer';
  }

  get color() {
    return this.getAttribute('color') || '#dddd55';
  }

  /**
   * @param {CanvasRenderingContext2D} ctx
   */
  render(ctx) {
    ctx.fillStyle = this.color;
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1 / 20;
    ctx.font = '20px monospace';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';

    ctx.translate(0.5, 0.5);

    ctx.beginPath();
    ctx.arc(0, 0, 0.4, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
  }
}

defineElement(RlkHeroRenderer);
