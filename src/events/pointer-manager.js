import { defineElement } from '../custom-elements';

export class RlkPointerManager extends HTMLElement {
  static get is() {
    return 'rlk-pointer-manager';
  }

  get canvas() {
    return document.querySelector(this.getAttribute('canvas'));
  }

  connectedCallback() {
    this.canvas.addEventListener('click', this.handleClick.bind(this));
  }

  handleClick(evt) {
    const { clientX, clientY } = evt;
    const {
      offsetLeft,
      offsetTop,
      centerX,
      centerY,
      cellWidth,
      cellHeight,
    } = this.canvas;

    const camera = this.canvas.getCameraPosition();

    const offsetX = clientX - offsetLeft - centerX;
    const offsetY = clientY - offsetTop - centerY;

    const x = Math.floor((offsetX / cellWidth) + camera.x);
    const y = Math.floor((offsetY / cellHeight) + camera.y);

    this.dispatchEvent(new CustomEvent('rlk:cell-press', { detail: { x, y }, bubbles: true }), this);
  }
}

defineElement(RlkPointerManager);
