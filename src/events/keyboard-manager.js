import { defineElement } from '../custom-elements';

const EVT_BY_KEYCODE = {
  ArrowUp: 'rlk:move-up',
  ArrowDown: 'rlk:move-down',
  ArrowLeft: 'rlk:move-left',
  ArrowRight: 'rlk:move-right',
};

export class RlkKeyboardManager extends HTMLElement {
  static get is() {
    return 'rlk-keyboard-manager';
  }

  connectedCallback() {
    document.addEventListener('keydown', evt => this.handleKey(evt.code));
  }

  handleKey(keyCode) {
    if (keyCode in EVT_BY_KEYCODE) {
      this.dispatchEvent(new CustomEvent(EVT_BY_KEYCODE[keyCode], { 'bubbles': true }), this);
    }
  }
}

defineElement(RlkKeyboardManager);
